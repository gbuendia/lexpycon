# Lexpycon

A small Python dictionary manager with an emphasis on (internal) etymology,
based on [PySimpleGUI](https://www.pysimplegui.org).

⚠⚠⚠ This is a personal project that is still far from being mature ⚠⚠⚠


## Description

While trying my hand at creating a 
[conlang](https://en.wikipedia.org/wiki/Constructed_language),
I couldn't find any tools that would suit my workflow in my need to keep 
track of the vocabulary, so I find myself now trying my hand at 
creating my first (serious) GUI app.

The app uses its own dictionary format based on [TOML](https://toml.io/), so
the data remains human readable in case Something Really Bad happens to the
app.


## Functionality and to-do list

- [x] Read a dictionary
  - [x] Display the list of lemmas in a list
  - [x] Display a selected entry's information
- [ ] Edit a dictionary
  - [x] Modify information related to the dictionary itself (except complex field, see below)
  - [x] Add new entry
  - [x] Remove selected entry
  - [ ] Modify information of selected entry
    - [x] Modify simple fields (headword, meaning...)
    - [ ] Add and remove elements from complex fields (etymology, grammatical features...)
	  - [x] Propagate changes to the dictionary configuration if needed
- [x] Export a dictionary into a printable format (basic PDF via markdown)
- [ ] Check a dictionary file for errors
- [ ] Documentation
  - [x] Readme (this one)
  - [ ] Detailed dictionary format documentation
  - [ ] User manual


## Installation.

1. Don't.

At least for the moment.


## Format

A dictionary is a TOML file comprising of the following tables and and keys:

```toml
[configuration]
    conlang_name = "Name of the conlang"
    # Parts of speech, for example:
    p_o_s = [
		["noun", "n.", ["gender", "class", "number"]],
		["verb", "v.", ["number", "aspect"]],
	]

	# Names of the historical periods
	periods = [
		["Example 1", -10000, -300],
		["Example 2", -299, 100],
		["Example 3", 101, 1000]
	]

# The features sub table is bound to be changed soon and will
# probably be a compatibility breaking change.
[configuration.features]
	gender = [
		["animate", "anim"],
		["inanimate", "inan"],
	]

	class = [
		["edible", "ed"],
		["inedible", "ined"]
	]

	number = [
		["singular", "sing"],
		["dual", "dual"],
		["plural", "pl"],
	]
```

Followed by the actual lemmata which comprise the following keys:

```toml
# Lemma ID, unique:
[xxxx]

headword = "thing"
p_o_s = "noun"
meaning = "Description of what a thing is."

# Pronunciation.
ipa = "θɪŋ"
# still undecided about using X-Sampa
xsampa = ""

# Period(s) in which the lemma was used with its current meaning/pronunciation
usage_from = -250
usage_until = 3

# Etymology
# IDs of existing lemmas where this lemma comes from
etyma = ["1hrc", "HvZg"]
ety_notes = """\
	A free text explaining the etymology \
	in case clarification is needed, or the \
	lemma comes from a word foreign to the dictionary. \
"""

grammar_notes = "Free text about grammar."
# Grammatical features.
features.gender = "animate"
features.class = "inedible"
```

A more thorough explanation of the format is on its way.