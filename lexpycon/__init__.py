"""Lexpycon.
A small Python dictionary manager with an emphasis on (internal) etymology."""

__version__ = "0.4.0"
