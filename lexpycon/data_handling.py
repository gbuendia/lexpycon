#!/usr/bin/env python3

"""Does the data loading, editing, etc for lexpycode."""

# Standard library
from dataclasses import dataclass, field
import logging
from math import inf
import random
import string

# Third party
from PySimpleGUI import popup_error
from rich.logging import RichHandler
import tomlkit as tk

########################################################################
# CONSTANTS - CONFIG                                                   #
########################################################################

# Silencing the verbosity of fontTools, imported by weasyPrint, imported by md2pdf
logging.getLogger('fontTools').setLevel(logging.WARNING)
logging.getLogger('weasyprint').setLevel(logging.WARNING)

FORMAT = "%(message)s"
logging.basicConfig(
    level="NOTSET", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)
log = logging.getLogger("rich")

########################################################################
# CLASSES                                                              #
########################################################################

@dataclass
class Lemma:
    """The combination of a headword with its usage details.
    Adding an ID to it results in a lexicon entry."""
    headword: str = field(
        metadata = { "ui": "-I-R-HEADW-" }
        )
    meaning: str = field(
        metadata = { "ui": "-I-R-MEANING-" }
        )
    ipa: str = field(
        default = "",
        metadata = { "ui": "-I-R-IPA-" }
        )
    xsampa: str = field(
        default = "",
        metadata = { "notes": "DEPRECABLE?" }
        )
    p_o_s: str = field(
        default = "",
        metadata = { "ui": "-I-R-POS-" }
        )
    usage_from: int = field(
        default = 0,
        metadata = { "ui": "-I-R-FROM-" }
        )
    usage_until: int = field(
        default = 0,
        metadata = { "ui": "-I-R-UNTIL-" }
        )
    etyma: list[str] = field(
        default_factory = lambda: [],
        metadata = { "ui": "-I-R-ETYTREE-" }
        )
    ety_notes: str = field(
        default = "",
        metadata = { "ui": "-M-R-ETYNOTES-" }
        )
    features: dict[str, str] = field(
        default_factory = lambda: {},
        metadata = { "ui": "-T-R-FEATS-" }
        )
    grammar_notes: str = field(
        default = "",
        metadata = { "ui": "-M-R-GRAMMAR-" }
        )

    def __str__(self):
        human_readable = f"{self.headword}"
        if self.ipa != "":
            human_readable += f" /{self.ipa}/"
        human_readable += "\n"
        if self.p_o_s != "":
            human_readable += f"{self.p_o_s}"
        if bool(self.features) is True:
            # Empty dicts evaluate to False
            human_readable += f", {self.features}"
        human_readable += f"\n{self.meaning}\n"
        # We accept periods from 0 to 0 as it can be wished by the user
        human_readable += f"{self.usage_from} ~ {self.usage_until}\n"
        if len(self.etyma) > 0:
            human_readable += f"{self.etyma}"
        if self.ety_notes != "":
            human_readable += f"{self.ety_notes}"
        if self.grammar_notes != "":
            human_readable += f"\n{self.grammar_notes}"
        return human_readable

    def update_simple(
        self,
        headword,
        meaning,
        ipa,
        p_o_s,
        usage_from,
        usage_until,
        ety_notes,
        grammar_notes,
        **kwargs
        ):
        """Updates a Lemma using the fields of a dict.
        The Finish edit button sends the totality of fields, always.
        Any extra arguments (eg. x_sampa) or the ones we don't deal with here
        (features, etyma) are 'discarded' into kwargs"""
        self.headword = headword
        self.meaning = meaning
        self.ipa = ipa
        self.p_o_s = p_o_s
        self.usage_from = usage_from
        self.usage_until = usage_until
        self.ety_notes = ety_notes
        self.grammar_notes = grammar_notes
        del kwargs

    def is_valid(self):
        """Checks that a lemma's attributes are the right type
        and whether they are non empty if required"""
        errors = []
        warnings = []

        def check_types(instance):
            valid = True
            # attributes = [[ attr.name, expected type, expected non-empty ]]
            attributes = [
                ["headword", str, True],
                ["meaning", str, True],
                ["ipa", str, False],
                ["p_o_s", str, False],
                ["features", dict, False],
                ["usage_from", int, False],
                ["usage_until", int, False],
                ["etyma", list, False],
                ["ety_notes", str, False],
                ["grammar_notes", str, False]
            ]
            nonlocal errors, warnings
            for attrib in attributes:
                if isinstance(getattr(instance, attrib[0]), attrib[1]) is False:
                    valid = False
                    errors.append(f"Attribute {attrib[0]} expected {attrib[1]}")
                elif bool(getattr(instance, attrib[0])) is False and attrib[2] is True:
                    warnings.append(f"Atribute {attrib[0]} is empty")
            return valid

        return check_types(self), warnings, errors

    def toml(self):
        """Returns a TOML object for the lemma."""
        toml_representation = tk.table()
        toml_representation.add(tk.nl())
        toml_representation.add("headword", self.headword)
        toml_representation.add(tk.nl())
        toml_representation.add("p_o_s", self.p_o_s)
        toml_representation.add(tk.nl())
        toml_representation.add("meaning", self.meaning)
        toml_representation.add(tk.nl())
        toml_representation.add("ipa", self.ipa)
        toml_representation.add(tk.nl())
        toml_representation.add("usage_from", self.usage_from)
        toml_representation.add("usage_until", self.usage_until)
        toml_representation.add(tk.nl())
        toml_representation.add("etyma", self.etyma)
        toml_representation.add("ety_notes", self.ety_notes)
        toml_representation.add(tk.nl())
        for feature, value in self.features.items():
            toml_representation.add(tk.key(['features', feature]), value)
        return toml_representation


def generate_id():
    """Returns a new lemma ID based on
    https://stackoverflow.com/a/56398787
    """
    alphabet = string.ascii_lowercase + string.ascii_uppercase + string.digits
    while True:
        entry_id = "".join(random.choices(alphabet, k=4))
        # Stay in the while loop till unique:
        if entry_id not in working_dict.lexicon:
            break
    return entry_id


@dataclass
class Entry:
    """Each one of the meaningful components of a lexicon.
    Consists of a lemma with a unique ID."""
    lemma: Lemma
    entry_id: str = field(
        default_factory = generate_id,
        metadata = { "ui": "-I-R-HEADW-" }
        )

    def is_valid(self):
        """Checks that an entry is well formed.
        Doesn't check ID uniqueness."""
        valid_lemma = self.lemma.is_valid()
        for message in valid_lemma[1]:
            log.warning(message)
        for message in valid_lemma[2]:
            log.error(message)
        return valid_lemma[0] and self.entry_id != ""

    def to_markdown(self, config):
        """Converts a single entry to MD."""
        md_template = (
            "### {headword}\n\n"
            "`/{pronunciation}/ {grammar}`\n\n"
            "{meaning}\n\n"
            "Usage: {usage} ({named_usage})."
            )

        def get_grammar(p_o_s, features, config):
            grammar_strings = []

            if p_o_s != "":
                n_or_a = ""
                for name_abbr in config.p_o_s:
                    if name_abbr[0] == p_o_s:
                        n_or_a = name_abbr[1]
                        break
                # If n_or_a remains as "" (i.e. falsy), use p_o_s:
                grammar_strings.append(n_or_a or p_o_s)

            if bool(features) is True:
                for feat, value in features.items():
                    n_or_a = ""
                    for name_abbr in config.features[feat]:
                        if name_abbr[0] == value:
                            n_or_a = name_abbr[1]
                            break
                    grammar_strings.append(n_or_a or p_o_s)

            return " ".join(grammar_strings)

        return md_template.format(
            headword = self.lemma.headword,
            grammar = get_grammar(self.lemma.p_o_s, self.lemma.features, config),
            pronunciation = self.lemma.ipa,
            meaning = self.lemma.meaning,
            usage = f"{self.lemma.usage_from} - {self.lemma.usage_until}",
            named_usage = get_period(self.lemma.usage_from, self.lemma.usage_until)
        )

    def toml(self):
        """Converts entry to TOML object."""
        entry_table = tk.table()
        entry_table.add(self.entry_id, self.lemma.toml())
        return entry_table


@dataclass
class Lexicon(list):
    """A list of lemmas identified by a unique ID (entries)."""

    def append(self, entry):
        """Adds an entry to the end of the lexicon."""
        log.info("Appending entry %s to lexicon", entry.entry_id)
        super().append(self._validate_entry(entry))

    def _validate_entry(self, entry):
        if not isinstance(entry, Entry):
            raise TypeError(
                f"ERROR: Expected a lexicon entry, got {type(entry).__name__}"
            )
        if not self.is_unique(entry.entry_id):
            raise RuntimeError(
                f"ERROR: ID {entry.entry_id} is not unique."
            )
        return entry

    def get(self, entry_id):
        """Gets a lemma given its entry_id."""
        for entry in self:
            if entry.entry_id == entry_id:
                return entry.lemma
        raise ValueError(
            f"ERROR: Couldn't find an entry with ID {entry_id}"
        )

    def _all_ids(self):
        all_ids = []
        for entry in self:
            all_ids.append(entry.entry_id)
        return all_ids

    def duplicates(self):
        """Gets a list of duplicate entry IDs"""
        log.info("Checking lexicon for duplicate IDs")
        seen = set()
        dupes = []
        for entry_id in self._all_ids():
            if entry_id in seen:
                dupes.append(entry_id)
            else:
                seen.add(entry_id)
        return dupes

    def is_unique(self, entry_id):
        """Checks if an entry ID has no duplicates in the lexicon."""
        log.debug("Checking if ID %s already exists", entry_id)
        if entry_id in self._all_ids():
            return False
        return True

    def _get_index(self, entry_id):
        """Gets the index in the list of an entry with a given entry_id."""
        for index, entry in enumerate(self):
            if entry.entry_id == entry_id:
                return index
        raise ValueError(
            f"ERROR: Couldn't find an entry with ID {entry_id}"
        )

    def update(self, entry_id, new_entry):
        """Gets each attribute of the new entry and assigns its 
        value to the same attribute of the corresponding entry in the
        working lexicon."""
        index = self._get_index(entry_id)
        log.info("Updating entry nº %d of lexicon (%s)", index, entry_id)
        for key in new_entry.lemma.__dict__:
            new_value = getattr(new_entry.lemma, key)
            log.debug("%s %s: %s > %s", entry_id, key, getattr(self[index].lemma, key), new_value)
            setattr(self[index].lemma, key, new_value)


    def del_entry(self, entry):
        """Removes entry from lexicon."""
        log.info("Removing %s from lexicon", entry.entry_id)
        # As lexicon is of type list, this should suffice:
        self.remove(entry)


    def __str__(self):
        """Human friendly printable form."""
        printable = ""
        if len(self) >= 3:
            printable += f"{self[0].entry_id}: {self[0].lemma.headword}"
            printable += "\n"
            printable += f"... {len(self) -2} other entries ..."
            printable += "\n"
            printable += f"{self[-1].entry_id}: {self[-1].lemma.headword}"
        else:
            for entry in self:
                printable += entry.lemma.__str__()
        return printable

    def ety_text(self, config, entry_id, lemma_end_usage, lines):
        """Recursively produces a string describing the 'parent' etyma."""
        log.info("Getting etymology text for %s", entry_id)

        def get_derivation_period(desc_date, etymon_id):
            log.debug("Deciding period name for either lemma or etymn (%s)", etymon_id)
            # we get the date for the former iteration
            # i.e. the start of usage of the lemma.
            # the current id is the etymon: if they don't
            # overlap, we assume a 'ressurection' of the etymon
            # so we use the 'until' date of the etymon instead.
            if etymon_id == "" or desc_date == "":
                return ""
            etymon_date = self.get(etymon_id).usage_until
            # We cannot derive a lemma with a start date prior
            # to the start date of the etymon:
            if self.get(etymon_id).usage_from > desc_date:
                log.critical(
                    "Etymon usage %s starts AFTER end of derived lemma %s",
                    self.get(etymon_id).usage_from,
                    desc_date
                )
                popup_error("ERROR\n\nEtymon usage starts after derived lemma")
                return ""
            if etymon_date < desc_date:
                using = etymon_date
            else:
                using = desc_date
            for period in config.periods:
                # Period is array of name, start, end
                # We'll use this period if the usage started
                # after the period start of before the period end.
                if period[1] <= using <= period[2]:
                    return period[0]

        # Each node is [ parent key, key, display text, value1, valuen... ]
        # We begin with an empty array as nodes
        text = lines
        lemma = working_dict.lexicon.get(entry_id)
        for etymon_id in lemma.etyma:
            etymon = working_dict.lexicon.get(etymon_id)
            line = f"from {get_derivation_period(lemma_end_usage, etymon_id)} "
            line += f"_{etymon.headword}_ '{etymon.meaning}', "
            text.append(line)
            self.ety_text(config, etymon_id, etymon.usage_until, text)
        return text

    def to_markdown(self, config, starting, ending, separator = "\n\n"):
        """Converts the whole lexicon to MD."""
        log.info("Converting the lexicon to markdown")
        entries_md = []
        for entry in self:
            if entry.lemma.usage_from >= starting and entry.lemma.usage_until <= ending:
                entry_md = entry.to_markdown(config)
                entry_md += "\n\n"
                ety = " ".join(self.ety_text(config, entry.entry_id, "", []))
                entry_md += f"{ety[:-2].capitalize()}."
            entries_md.append(entry_md)
        markdown = separator.join(entries_md)
        return markdown

    def toml(self):
        """Returns a list of the TOML representation of all the entries."""
        log.info("Converting lexicon entries to TOML")
        entry_tables = []
        for entry in self:
            entry_tables.append(entry.toml())
        return entry_tables


@dataclass
class DictConfig:
    """Stores information about a dictionary and some data
    the entries can refer to."""
    author: str = field(
        default = "Unknown"
    )
    conlang_name: str = field(
        default = "No Name"
    )
    conlang_description: str = field(
        default = ""
    )
    p_o_s: list = field(
        default_factory = lambda: []
    )
    periods: list = field(
        default_factory = lambda: []
    )
    features: dict[str, list] = field(
        default_factory = lambda: {}
    )

    def update(self, new_config):
        """Gets each attribute of the new config and assigns its 
        value to the same attribute of config in the
        working lexicon."""
        log.info("Updating dictionary config")
        for key in new_config.__dict__:
            new_value = getattr(new_config, key)
            log.debug("Config %s: %s > %s", key, getattr(self, key), new_value)
            setattr(self, key, new_value)

    def ordered_abbr(self):
        """Returns an ordered list of the abbreviations
        used in a dictionary and their meanings."""
        log.info("Getting the list of the abbreviations used in the dictionary")
        grammar = []
        for part in self.p_o_s:
            grammar.append([part[1], part[0]])
        for feature, values in self.features.items():
            for value in values:
                grammar.append([value[1], f"{value[0]} {feature}"])
        log.debug("Abbreviations: %s", grammar)
        return sorted(grammar, key=lambda row: row[0])

    def toml(self):
        """Returns a TOML object for the dictionary configuration."""
        log.info("Converting configuration to TOML")
        config_table = tk.table()
        config_table.add(tk.nl())
        config_table.add("author", self.author)
        config_table.add("conlang_name", self.conlang_name)
        config_table.add("conlang_description", self.conlang_description)
        config_table.add("p_o_s", self.p_o_s)
        config_table.add("periods", self.periods)
        feats_table = tk.table()
        feats_table.add(tk.nl())
        for feature, values in self.features.items():
            feats_table.add(feature, values)
        config_table.add("features", feats_table)
        return config_table


@dataclass
class Dictionary:
    """A document containing a lexicon and information about itself."""
    config: DictConfig
    lexicon: Lexicon

    def to_markdown(self, separator, starting = -inf, ending = inf):
        """Converts the whole dictionary to MD."""
        log.info("Converting dictionary to markdown")
        markdown = f"# {self.config.conlang_name} Dictionary\n\n"
        markdown += "## Preface\n\n"
        markdown += "### Abbreviations\n\n"
        for abbr in self.config.ordered_abbr():
            markdown += f"* **{abbr[0]}**: {abbr[1]}\n"
        markdown += "\n\n---\n\n## Lexicon\n\n"
        markdown += self.lexicon.to_markdown(self.config, starting, ending, separator)
        return markdown

    def toml(self):
        """Returns a TOML object for the whole dictionary."""
        log.info("Converting dictionary to TOML.")
        toml_representation = tk.document()
        toml_representation.add(tk.comment("Data file for Lexpycon"))
        toml_representation.add(tk.nl())
        toml_representation.add("configuration", self.config.toml())
        toml_representation.add(tk.nl())
        toml_representation.add(
            tk.comment("##########################################################")
            )
        toml_representation.add(
            tk.comment("                      Lemmata list                       #")
            )
        toml_representation.add(
            tk.comment("##########################################################")
            )
        toml_representation.add(tk.nl())
        first_lemma = True
        for entry in self.lexicon.toml():
            if first_lemma is True:
                first_lemma = False
            else:
                toml_representation.add(
                    tk.comment("##########################################################")
                    )
            for entry_id, lemma in entry.items():
                toml_representation.add(entry_id, lemma)
            toml_representation.add(tk.nl())
        return toml_representation


########################################################################
# GLOBALS                                                              #
########################################################################

working_file = ""
working_dict = Dictionary(DictConfig(), Lexicon())
working_entry = Entry(Lemma("", ""))
unsaved_changes = False


########################################################################
# DATA HANDLING                                                        #
########################################################################

def load_dict():
    """Loads the full dictionary data from a TOML file."""
    log.info("Opening dict file %s", working_file)
    try:
        with open(working_file, "r", encoding="UTF-8") as data_file:
            data = data_file.read()
            dict_tuple = get_config_and_lexicon(data)
    except:
        log.critical("An error occured while loading or parsing the dictionary file")
        popup_error("An error occured while loading or parsing the dictionary file")
        dict_tuple = None, None
    return dict_tuple


def valid_entries(entries):
    """Basic validation of dictionary entries well-formedness."""
    log.info("Validating lexicon")
    good_entries = Lexicon()
    all_valid = True
    for entry_id in entries:
        entry = Entry(entry_id = entry_id, lemma = Lemma(**entries[entry_id]))
        valid_lemma = entry.lemma.is_valid()
        if valid_lemma[0] is False:
            log.critical("Found incorrect lemma in entry %s", entry)
            for error in valid_lemma[2]:
                log.error(error)
            all_valid = False
        else:
            good_entries.append(entry)
            if len(valid_lemma[1]) > 0:
                log.warning("Got warnings for lemma in entry %s", entry)
                for warning in valid_lemma[1]:
                    log.warning(warning)
    if all_valid is True:
        log.info("All entries passed")
        return good_entries
    return None


def get_config_and_lexicon(text):
    """Basic validation of dictionary data well-formedness
    and separation of configuration and entries."""
    log.info("Validating dictionary")
    try:
        dictionary = tk.parse(text)
    except:
        log.critical("Couldn't parse dictionary")
        popup_error("An error occured while parsing the dictionary contents")
        config, entries = None, None
    if "configuration" in dictionary:
        config = dictionary.pop("configuration")
        # Once popped, 'dictionary' contains only entries
        entries = valid_entries(dictionary)
        log.info("Validated %s entries", len(entries))
        duplicates = entries.duplicates()
        if len(duplicates) > 0:
            raise RuntimeError(
            f"ERROR: Duplicate IDs found {duplicates}"
        )
    return config, entries


def search_match(search, headword, meaning, in_headwords, in_meanings, case_sensitive):
    """Checks wheteher a certain lemma matches the search criteria."""
    does_match = False
    if case_sensitive is False:
        search = search.lower()
        headword = headword.lower()
        meaning = meaning.lower()
    if (in_headwords is True and search in headword) or (in_meanings is True and search in meaning):
        does_match = True
    return does_match


def get_period(start, end):
    """Returns the names of the first and last
    periods in the span of a lemma's usage."""
    periods = []
    # A lemma belongs to all periods that:
    # - ended after the first usage and
    # - started before the last usage
    # We order the period list to just pick
    # the first and last if the lemma spans
    # more than two.
    ordered = sorted(working_dict.config.periods, key=lambda x: x[1])
    # log.info("Getting usage period info") # too much info.
    for period in ordered:
        # Period is array of name, start, end
        if start <= period[2] and end >= period[1]:
            periods.append(period[0])
    if len(periods) > 2:
        periods = [periods[0], periods[-1]]
    # If periods has one item, join() has no visible effect:
    return " - ".join(periods)


def get_feats(entry_id):
    """Returns a list of the grammatical features of a lemma."""
    log.info("Getting grammatical features for lemma id %s", entry_id)
    lemma = working_dict.lexicon.get(entry_id)
    feats = []
    for feat in lemma.features:
        feats.append([feat, lemma.features[feat]])
    log.debug("Found %s", feats)
    return feats


def get_ety_tree(arrays, entry_id, descendant, end_usage):
    """Returns an array of arrays representing nodes
    ready to be converted into a PySimpleGUI TreeData object."""
    log.info("Getting etymology tree for %s", entry_id)

    def get_derivation_period(desc_date, etymon_id):
        log.debug("Getting period name for either %s or %s", desc_date, etymon_id)
        # we get the date for the former iteration
        # i.e. the start of usage of the lemma.
        # the current id is the etymon: if they don't
        # overlap, we assume a 'ressurection' of the etymon
        # so we use the 'until' date of the etymon instead.
        if etymon_id == "" or desc_date == "":
            return ""
        etymon_date = working_dict.lexicon.get(etymon_id).usage_until
        # We cannot derive a lemma with a start date prior
        # to the start date of the etymon:
        if working_dict.lexicon.get(etymon_id).usage_from > desc_date:
            log.critical(
                "Etymon usage %s starts AFTER end of derived lemma %s",
                working_dict.lexicon.get(etymon_id).usage_from,
                desc_date
            )
            popup_error("ERROR\n\nEtymon usage starts after derived lemma")
            return ""
        if etymon_date < desc_date:
            using = etymon_date
        else:
            using = desc_date
        for period in working_dict.config.periods:
            # Period is array of name, start, end
            # We'll use this period if the usage started
            # after the period start of before the period end.
            if period[1] <= using <= period[2]:
                return period[0]

    # Each node is [ parent key, key, display text, value1, valuen... ]
    # We begin with an empty array as nodes
    nodes = arrays
    lemma = working_dict.lexicon.get(entry_id)
    node = []
    node.append(descendant) # if parent is "" psg.Tree understands it as top level
    node.append(entry_id)
    node.append(lemma.headword)
    node.append(get_derivation_period(end_usage, entry_id))
    node.append(lemma.meaning)
    nodes.append(node)
    for etymon in lemma.etyma:
        # Now we look for the etyma of each etymon of the lemma we just added
        # so the etymon is now the id to get a tree for, and the original id
        # is treated as descendant to anchor the next node.
        # We'll use the start of usage for the lemma as the period for the etymon
        if etymon != "":
            get_ety_tree(nodes, etymon, entry_id, working_dict.lexicon.get(entry_id).usage_until)
    return nodes


########################################################################
########################################################################


def main():
    """I don't really know if I need a main() funtion here."""
    print("Data handling file fot Lexpycon. To be imported.")


if __name__ == '__main__':
    main()
