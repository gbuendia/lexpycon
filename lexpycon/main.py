#!/usr/bin/env python3

"""Main module of Lexpycon.
Handles the GUI events."""

# Standard library
import json
import logging
from os import chdir
from os.path import expanduser, isfile
from webbrowser import open as wo
try:
    from sys import _MEIPASS
    PACKAGED = True
except ImportError:
    PACKAGED = False


# Third party
from md2pdf.core import md2pdf # https://github.com/jmaupetit/md2pdf
import psg_reskinner as reskin
import PySimpleGUI as psg
from rich.logging import RichHandler
import tomlkit as tk

# Local
import __init__
import data_handling as dh
from layout import main_layout
import licenses


########################################################################
# CONSTANTS - CONFIG                                                   #
########################################################################

SETTINGS_FILE = expanduser("~/.local/share/lexpycon/settings.json")

SHOW_ALL_VALUES = False

PROJECT_URL = "https://gitlab.com/gbuendia/lexpycon"


def get_version():
    """Gets the version number for the module."""
    return __init__.__version__


VERSION_STRING = (
    f"Lexpycon version {get_version()}\n\n"
    f"Using PySimpleGUI version {psg.version}\n"
    f"Using tomlkit version {tk.__version__}\n\n"
)


# Silencing the verbosity of fontTools, imported by weasyPrint, imported by md2pdf
logging.getLogger('fontTools').setLevel(logging.WARNING)
logging.getLogger('weasyprint').setLevel(logging.WARNING)

FORMAT = "%(message)s"
logging.basicConfig(
    level="NOTSET", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)
log = logging.getLogger("rich")

# Needed to find the data added to the exe:
if PACKAGED is True:
    chdir(_MEIPASS) # https://stackoverflow.com/a/57481331
# Files included by pyinstaller will be found referencing them
# like "./file.ext"

psg.set_options(font=('mono', 10))


########################################################################
# DEFINITIONS                                                          #
########################################################################

def confirm_unsaved(function = None, window = None):
    """Asks the user for confirmation to perform an action."""
    if dh.unsaved_changes is True:
        log.warning("Attempting to perform %s with unsaved changes", (function or 'action'))
        confirm = psg.popup_ok_cancel(
            "Discard unsaved changes?",
            title = "There are unsaved changes",
            keep_on_top = True
        )
        if confirm.lower() == "ok":
            log.info("User confirmed discard changes")
            dh.unsaved_changes = False
            if function is not None:
                function(window)
            else:
                return True
        else:
            log.warning("User cancelled action")
    else:
        log.info("No unsaved changes, going forward")
        if function is not None:
            function(window)
        else:
            return True


def clean_window(window):
    """Empties all the fields that show dictionary or lemma information"""
    log.info("Cleaning window")
    for element in window.element_list():
        element_type = type(element).__name__
        if element.key not in ["-I-FILE-", "-LEXTABLE-", None]:
            if element_type in ["Input", "Multiline"]:
                window[element.key].update(value = "")
                log.debug("%s %s clear", element_type, element.key)
            elif element_type == "Tree":
                window[element.key].update(values = psg.TreeData())
                log.debug("%s %s clear", element_type, element.key)
            elif element_type == "Table":
                window[element.key].update(values = [])
                log.debug("%s %s clear", element_type, element.key)


def set_theme(window, theme = "GreenMono"):
    """Changes the colour theme of the GUI."""
    log.info("Changing window theme to %s", theme)
    # For some reason Table and Text stay the same colour
    # This fixes table:
    psg.theme(theme) # otherwise psg.theme_background_color() doesn't change
    window["-LEXTABLE-"].BackgroundColor = psg.theme_background_color()
    window["-T-R-FEATS-"].BackgroundColor = psg.theme_background_color()
    # This doesn't fix Text:
    for element in window.element_list():
        if 'text' in str(type(element)).lower():
            element.BackgroundColor = psg.theme_background_color()
            element.background_color = psg.theme_background_color()
    # This works better than psg.theme() alone:
    reskin.reskin(
        window=window,
        new_theme=theme,
        theme_function=psg.theme,
        lf_table=psg.LOOK_AND_FEEL_TABLE,
    )
    window.metadata = theme


def save_settings(window):
    """Saves app settings, not data."""
    settings = {
        "file": dh.working_file,
        "theme": window.metadata
    }
    log.info("Saving settings:")
    log.debug("Values: %s", settings)
    with open(SETTINGS_FILE, "w", encoding="UTF-8") as settings_file:
        json.dump(settings, settings_file)
    log.info("Settings saved to %s", SETTINGS_FILE)


def load_settings(window):
    """Loads app settings."""
    log.info("Loading settings...")
    with open(SETTINGS_FILE, "r", encoding="UTF-8") as settings_file:
        settings = json.load(settings_file)
    log.info("Settings: %s", settings)
    set_theme(window, settings["theme"])
    dh.working_file = settings["file"]


def initialize_window(window):
    """Clears window, populates title, and left side according to data file chosen."""
    confirm_unsaved(clean_window, window)
    dh.working_dict.config = dh.DictConfig(**dh.load_dict()[0])
    dh.working_dict.lexicon = dh.load_dict()[1]
    window["-TITLE-"].update(value = dh.working_dict.config.conlang_name)
    window.set_title(f"Lexpycon {dh.working_dict.config.conlang_name}")
    window["-L-T-FILE-"].update(value = dh.working_file)
    populate_table(window)
    populate_config(window, dh.working_dict.config)


def edit_mode(window, panel, mode):
    """Switches right side fields between active or inactive for editing."""
    for element in window.element_list():
        ename = str(element.__class__.__name__).lower()
        if ename in ["input", "multiline"] and element.key.split("-")[2] == panel:
            element.update(disabled = not mode)
    if panel == "R":
        window["-O-R-POS-"].update(visible = mode)
        for frame in ["-F-EDIT-FEAT-", "-F-EDIT-ETY-"]:
            window[frame].update(visible = mode)
        for button in ["-B-FEATDEL-", "-B-ETYDEL-"]:
            window[button].update(disabled = True)
        if mode is True:
            window["-B-EDIT-"].update(text = "Finish")
        else:
            window["-B-EDIT-"].update(text = "Edit")
    if panel == "T":
        window["-F-PEDIT-"].update(visible = mode)
        window["-B-PDEL-"].update(disabled = True)
        if mode is True:
            window["-B-PEDIT-"].update(text = "Finish")
        else:
            window["-B-PEDIT-"].update(text = "Edit")


def populate_config(window, config):
    """Fills the top section of the window with information
    about the dictionary in use."""
    edit_mode(window, "T", False)
    log.debug("Pupulating top panel with \n%s", config)
    window["-I-T-AUTHOR-"].update(value = config.author)
    window["-I-T-LANGNAME-"].update(value = config.conlang_name)
    window["-I-T-DESC-"].update(value = config.conlang_description)
    combo = [ part[0] for part in config.p_o_s ]
    combo.append("Add new...")
    window["-O-R-POS-"].update(values = combo)
    rows = []
    for period in config.periods:
        rows.append(period)
    window["-T-T-PERIODS-"].update(values = rows)


def populate_table(window):
    """Fills each row in the left side table with
    the id (invisible), headword, period and meaning
    of every lemma in the global lexicon that matches 
    the search conditions."""
    log.debug("Populating table with lexicon")
    search = window["-I-L-SEARCH-"].get()
    rows = []
    sorted_lex = sorted(
        dh.working_dict.lexicon,
        key=lambda entry: entry.lemma.headword
    )
    for entry in sorted_lex:
        if dh.search_match(
            search,
            entry.lemma.headword,
            entry.lemma.meaning,
            window["-C-L-SHW-"].get(),
            window["-C-L-SMN-"].get(),
            window["-C-L-SCS-"].get()
            ):
            rows.append(
                [entry.entry_id, entry.lemma.headword,
                dh.get_period(
                    entry.lemma.usage_from,
                    entry.lemma.usage_until),
                entry.lemma.meaning]
        )
    window["-LEXTABLE-"].update(values = rows)
    window["-B-DEL-"].update(disabled = True)


def ety_treedata(arrays):
    """Converts each array in the data returned by dh.get_ety_tree
    into a PySimpleGUI node of a TreeData object."""
    treedata = psg.TreeData()
    for arr in arrays:
        # Each node is [ parent key, key, display text, [value1, valuen...] ]
        treedata.insert(
            arr[0],
            arr[1],
            arr[2],
            arr[3:]
        )
    return treedata


def populate_entry(window, entry_id, lemma):
    """Takes the lemma corresponding to the lexicon entry of a certain ID
    and shows its information in the right side."""
    log.info("Showing entry for lemma %s", entry_id)
    edit_mode(window, "R", False)
    window["-B-EDIT-"].update(disabled = False)
    window["-I-R-ID-"].update(value = entry_id)
    window["-I-R-HEADW-"].update(value = lemma.headword)
    window["-I-R-IPA-"].update(value = lemma.ipa)
    window["-I-R-MEANING-"].update(value = lemma.meaning)
    window["-I-R-POS-"].update(value = lemma.p_o_s)
    window["-I-R-FROM-"].update(value = lemma.usage_from)
    window["-I-R-UNTIL-"].update(value = lemma.usage_until)
    window["-T-R-PERIOD-"].update(
        value = dh.get_period(
            lemma.usage_from,
            lemma.usage_until
            )
        )
    window["-T-R-FEATS-"].update(values = dh.get_feats(entry_id))
    window["-M-R-GRAMMAR-"].update(value = lemma.grammar_notes)
    window["-M-R-ETYNOTES-"].update(value = lemma.ety_notes)
    # Adding etyma list to metadata to not have to convert from datatree
    # upon collecting the data from the fields.
    window["-I-R-ETYTREE-"].metadata = { "ety": lemma.etyma }
    window["-I-R-ETYTREE-"].update(
        values = ety_treedata(
            # The first iteration of the tree has
            # no descendants and no date as the
            # period is already shown firther up in the interface.
            dh.get_ety_tree(
                [],
                entry_id,
                "",
                ""
            )
        )
    )


def collect_config(window):
    """Gets the attributes of the working dictionary
    from their fields in the GUI."""
    text_fields = [
        "-I-T-AUTHOR-",
        "-I-T-LANGNAME-",
        "-I-T-DESC-"
    ]
    # We don't edit p_o_s or features in this panel
    new_config = dh.DictConfig(
        p_o_s = dh.working_dict.config.p_o_s,
        features = dh.working_dict.config.features
    )
    for field in text_fields:
        attribute = window[field].metadata["attr"].split(".")[-1]
        setattr(new_config, attribute, window[field].get())
    periods = []
    for period in window["-T-T-PERIODS-"].get():
        periods.append(period)
    new_config.periods = periods
    log.info("Collected config data")
    log.debug(new_config)
    return new_config


def collect_entry(window):
    """Gets the attributes of the entry that is being shown
    from their fields in the GUI."""
    text_fields = [
        "-I-R-HEADW-",
        "-I-R-IPA-",
        "-I-R-MEANING-",
        "-I-R-POS-",
        "-M-R-GRAMMAR-",
        "-M-R-ETYNOTES-"
    ]
    new_lemma = dh.Lemma("", "")
    new_entry = dh.Entry(new_lemma, window["-I-R-ID-"].get())
    for field in text_fields:
        attribute = window[field].metadata["attr"].split(".")[-1]
        setattr(new_entry.lemma, attribute, window[field].get())
    try:
        usage_from = int(window["-I-R-FROM-"].get())
    except ValueError:
        log.error("Usage_from must be integer.")
        psg.popup_error("'Usage from' must be an integer.")
        usage_from = 0
        window["-I-R-FROM-"].update(value = "0")
    try:
        usage_until = int(window["-I-R-UNTIL-"].get())
    except ValueError:
        log.error("Usage_until must be integer.")
        psg.popup_error("'Usage until' must be an integer.")
        usage_until = 0
        window["-I-R-UNTIL-"].update(value = "0")
    new_entry.lemma.usage_from = usage_from
    new_entry.lemma.usage_until = usage_until
    features = {}
    for feature in window["-T-R-FEATS-"].get():
        features.update({feature[0]: feature[1]})
    new_entry.lemma.features = features
    new_entry.lemma.etyma = window["-I-R-ETYTREE-"].metadata["ety"]
    log.info("Collected entry data")
    log.debug(new_entry)
    return new_entry


########################################################################
########################################################################

def handle_event(event, values, window):
    """Handles clicks on the app window."""

    ########################################################### MENU
    # ----------------- FILE
    # NEW
    if event == "New...":
        file_path = psg.popup_get_file(
            "Please choose a file to save to",
            title = "Save as...",
            default_path = expanduser("~"),
            default_extension = ".toml",
            save_as = True
        )
        if file_path is None:
            log.info("File choice cancelled by user.")
        else:
            with open(file_path, "w", encoding = "UTF-8") as _:
                # Just 'touching' the new empty file
                pass
            if confirm_unsaved() is True:
                dh.working_file = file_path
                dh.working_dict = dh.Dictionary(dh.DictConfig(), dh.Lexicon())
                clean_window(window)
                populate_config(window, dh.working_dict.config)
                populate_table(window)
    # OPEN
    if event == "Open...":
        file_path = psg.popup_get_file(
            "Open dictionary data file",
            default_path = expanduser("~"),
            file_types = (("TOML Files", "*.toml"),)
        )
        if file_path is None:
            log.info("File choice cancelled by user.")
        elif isfile(file_path):
            log.info("Choosen data file:  %s", file_path)
            dh.working_file = file_path
            initialize_window(window)
        else:
            log.error("Chosen path is not a file")
            psg.popup_error("The chosen path doesn't look like a valid file.")

    # SAVE
    if event in ["Save", "Save as..."]:
        if event == "Save as...":
            file_path = psg.popup_get_file(
                "Please choose a file to save to",
                title = "Save as...",
                default_path = expanduser("~"),
                default_extension = ".toml",
                save_as = True
            )
        else:
            file_path = dh.working_file
        if file_path is None:
            log.info("File choice cancelled, nothing saved")
        elif isfile(file_path):
            log.info("Saving data to %s", file_path)
            converted = dh.working_dict.toml()
            try:
                with open(file_path, "w", encoding="UTF-8") as data_file:
                    data_file.write((tk.dumps(converted)))
                dh.unsaved_changes = False
                log.info("Data was successfully saved.")
            except:
                log.critical("Couldn't write data to file %s", file_path)
                psg.popup_error("An error accured while attempting to save.")
        else:
            log.error("%s is not a file", file_path)
            psg.popup_error("Choosen path doesn't look like a file")

    # EXPORT PDF
    if event == "Export as PDF...":
        log.info("Preparing PDF")
        output_file = psg.popup_get_file(
            "Please choose output file",
            title = "Save as PDF",
            save_as = True,
            keep_on_top = True
        )
        markdown = dh.working_dict.to_markdown("\n\n")
        log.info("Saving PDF to %s", output_file)
        try:
            md2pdf(
                output_file,
                md_content = markdown
            )
            log.info("Done")
        except:
            log.critical("PDF generation/saving failed")
            psg.popup_error("PDF generation/saving failed")

    # ----------------- WINDOW
    if "Colour theme" in event:
        theme = event.split(" ")[-1]
        set_theme(window, theme)
    # ----------------- HELP
    if event == "About":
        psg.popup_ok(VERSION_STRING, no_titlebar = True)
    if event == "Project webpage":
        wo(PROJECT_URL)
    if "LGPL" in event:
        psg.popup_scrolled(licenses.LGPL, size = (80, 22))
    if "MIT" in event:
        psg.popup_scrolled(licenses.MIT, size = (80, 22))

    ############################################################ TOP
    # ----------------- EDIT
    if event == "-B-PEDIT-":
        b_text = window["-B-PEDIT-"].get_text()
        if b_text == "Edit":
            edit_mode(window, "T", True)
        elif b_text == "Finish":
            edit_mode(window, "T", False)
            new_config = collect_config(window)
            if new_config != dh.working_dict.config:
                log.warning("Config differs from the one in the working dictionary.")
                log.debug("Previous config:")
                log.debug(dh.working_dict.config)
                dh.unsaved_changes = True
                dh.working_dict.config.update(new_config)
                log.info("Updated dictionary config")
                populate_config(window, dh.working_dict.config)
            else:
                log.info("No changes in config detected.")
        else:
            raise RuntimeError(
                "Event handler expected either EDIT of FINISH on button and got none."
            )
    # ----------------- PERIODS
    if event[:2] == ("-T-T-PERIODS-", "+CLICKED+"):
        clicked = event[2][0]
        if clicked is not None:
            row = window["-T-T-PERIODS-"].get()[clicked]
            log.info("Clicked -FEATURES- on row %s", clicked)
            window["-B-PDEL-"].update(disabled = False)
        else:
            log.info("Clicked -PERIODS- on an empty row")
    # ----------------- PERIOD DEL
    if event == "-B-PDEL-":
        selected_rows = values["-T-T-PERIODS-"]
        log.info("Selected period row/s %s to delete.", selected_rows)
        for row in selected_rows:
            values = window["-T-T-PERIODS-"].get()
            del values[row]
        window["-T-T-PERIODS-"].update(values = values)
        log.info("Row deleted")
        window["-B-PDEL-"].update(disabled = True)
    # ----------------- PERIOD ADD
    if event == "-B-PADD-":
        log.info("Getting new period data")
        button, values = psg.Window(
            "Please enter the period's data",
            [
                [psg.Text("Name"), psg.Input(key="-P-PNAME-")],
                [psg.Text("From"), psg.Input(key="-P-PFROM-")],
                [psg.Text("Until"), psg.Input(key="-P-PUNTIL-")],
                [psg.OK(), psg.Cancel()]
            ],
            disable_close=True
        ).read(close=True)
        if button == "Cancel":
            log.info("Period input cancelled by user")
        else:
            try:
                pfrom = int(values["-P-PFROM-"])
                puntil = int(values["-P-PUNTIL-"])
                values_are_int = True
            except:
                values_are_int = False
            if values_are_int is True and pfrom < puntil:
                new_period = [values["-P-PNAME-"], pfrom, puntil]
                periods = window["-T-T-PERIODS-"].get()
                window["-T-T-PERIODS-"].update(values = periods.append(new_period))
                log.info("New row added to periods")
            else:
                log.error("%d and %d are either not integers or not ordered.", pfrom, puntil)
                psg.popup_error(
                    f"Either {pfrom} or {puntil} are not integers"
                    "or the start is equal or bigger than the end date."
                    )

    ########################################################### LEFT
    # ----------------- SEARCH
    if event in ["-I-L-SEARCH-", "-C-L-SHW-", "-C-L-SMN-", "-C-L-SCS-"]:
        populate_table(window)
    # ----------------- LEXTABLE
    if event[:2] == ("-LEXTABLE-", "+CLICKED+"):
        clicked = event[2][0]
        if clicked is not None:
            row = window["-LEXTABLE-"].get()[clicked]
            log.info("Clicked -LEXTABLE- on row %s", clicked)
            log.debug("Row contents: %s", row)
            clicked_id = row[0]
            window["-B-DEL-"].update(disabled = False)
            clicked_lemma = dh.working_dict.lexicon.get(clicked_id)
            dh.working_entry.entry_id = clicked_id
            dh.working_entry.lemma = clicked_lemma
            populate_entry(window, clicked_id, clicked_lemma)
        else:
            log.info("Clicked -LEXTABLE- on an empty row")
    # ----------------- NEW ENTRY
    if event == "-B-NEW-":
        log.info("Appending blank entry to lexicon")
        new_entry = dh.Entry(dh.Lemma("NewEntry", ""))
        dh.unsaved_changes = True
        dh.working_entry = new_entry
        dh.working_dict.lexicon.append(new_entry)
        populate_entry(window, new_entry.entry_id, new_entry.lemma)
    # ----------------- REMOVE ENTRY
    if event == "-B-DEL-":
        log.info("Removing entry")
        dh.working_dict.lexicon.del_entry(dh.working_entry)
        clean_window(window)
        populate_table(window)
        dh.unsaved_changes = True

    ########################################################## RIGHT
    # ----------------- EDIT
    if event == "-B-EDIT-":
        b_text = window["-B-EDIT-"].get_text()
        if b_text == "Edit":
            edit_mode(window, "R", True)
        elif b_text == "Finish":
            edit_mode(window, "R", False)
            new_entry = collect_entry(window)
            if new_entry != dh.working_entry:
                log.warning("Entry differs from the one in the working lexicon.")
                log.debug("Previous entry:")
                log.debug(dh.working_entry)
                dh.unsaved_changes = True
                dh.working_dict.lexicon.update(dh.working_entry.entry_id, new_entry)
                log.info("Updated entry %s", dh.working_entry.entry_id)
                log.debug(dh.working_dict.lexicon.get(dh.working_entry.entry_id))
                dh.working_entry = new_entry
                populate_entry(window, dh.working_entry.entry_id, dh.working_entry.lemma)
            else:
                log.info("No changes in entry detected.")
        else:
            raise RuntimeError(
                "Event handler expected either EDIT of FINISH on button and got none."
            )
    # ----------------- POS
    if event == "-O-R-POS-":
        choice = values["-O-R-POS-"]
        if choice == "Add new...":
            button, values = psg.Window(
                "Please enter the part of speech data",
                [
                    [psg.Text("Name"), psg.Input(key="-P-POSNAME-")],
                    [psg.Text("Abbreviation"), psg.Input(key="-P-POSABBR-")],
                    [psg.OK(), psg.Cancel()]
                ],
                disable_close=True
            ).read(close=True)
            if button == "Cancel":
                log.info("PoS input cancelled by user")
            else:
                if values["-P-POSNAME-"] != "" and values["-P-POSABBR-"] != "":
                    window["-I-R-POS-"].update(value = values["-P-POSNAME-"])
                    dh.working_dict.config.p_o_s.append(
                        [
                            values["-P-POSNAME-"],
                            values["-P-POSABBR-"]
                        ]
                    )
                    log.info("Added new p.o.s '%s' to dictionary", values["-P-POSNAME-"])
                    dh.unsaved_changes = True
                else:
                    psg.popup_error("PoS name and abbreviation shouldn't be empty")
                    log.error("User entered empty PoS name and/or PoS abbreviation")
        elif choice != "":
            log.info("P.o.S. filled directly from combo box")
            window["-I-R-POS-"].update(value = choice)
    # ----------------- FEAT LIST
    if event[:2] == ("-T-R-FEATS-", "+CLICKED+"):
        clicked = event[2][0]
        if clicked is not None:
            row = window["-T-R-FEATS-"].get()[clicked]
            log.info("Clicked -FEATURES- on row %s", clicked)
            window["-B-FEATDEL-"].update(disabled = False)
        else:
            log.info("Clicked -FEATURES- on an empty row")
    # ----------------- ETY TREE
    if event == "-I-R-ETYTREE-":
        clicked = values["-I-R-ETYTREE-"]
        if clicked is not None:
            log.info("Clicked -ETYTREE- on row %s", clicked)
            ety_list = window["-I-R-ETYTREE-"].metadata["ety"]
            if set(clicked).issubset(set(ety_list)):
                window["-B-ETYDEL-"].update(disabled = False)
            else:
                window["-B-ETYDEL-"].update(disabled = True)
        else:
            log.info("Clicked -ETYTREE- on an empty row")
    # ----------------- FEAT DEL
    if event == "-B-FEATDEL-":
        selected_rows = values["-T-R-FEATS-"]
        log.info("Selected features row/s %s to delete.", selected_rows)
        for row in selected_rows:
            values = window["-T-R-FEATS-"].get()
            del values[row]
        window["-T-R-FEATS-"].update(values = values)
        log.info("Row deleted")
        window["-B-FEATDEL-"].update(disabled = True)
    # ----------------- ETY DEL
    if event == "-B-ETYDEL-":
        selected_rows = values["-I-R-ETYTREE-"]
        log.info("Selected etyma row/s %s to delete.", selected_rows)
        if selected_rows is not None:
            ety_list = window["-I-R-ETYTREE-"].metadata["ety"]
            if set(selected_rows).issubset(set(ety_list)):
                new_ety_list = [etymon for etymon in ety_list if etymon not in selected_rows]
                window["-I-R-ETYTREE-"].metadata = { "ety": new_ety_list}
                dh.working_entry.lemma.ety_list = new_ety_list
                psg.popup_ok("Tree will be updated after finishing the edition.")
                window["-B-ETYDEL-"].update(disabled = True)
        else:
            log.info("Clicked -ETYTREE- on an empty row")



def main():
    """Window loop listening for events."""
    window = psg.Window(
        "Lexpycon",
        main_layout,
        finalize = True,
        resizable = True
    )

    load_settings(window)
    initialize_window(window)
    set_theme(window, window.metadata)

    while True:
        event, values = window.read()
        log.info("Event %s", event)
        if SHOW_ALL_VALUES:
            log.debug(values)
        if event in (psg.WIN_CLOSED, "Exit"):
            if confirm_unsaved() is True:
                save_settings(window)
                log.info("Goodbye.")
                break
        handle_event(event, values, window)

    window.close()


if __name__ == '__main__':
    main()
