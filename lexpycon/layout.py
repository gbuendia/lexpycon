#!/usr/bin/env python3

"""Definition of the layout for PSG to draw the GUI."""

# Standard library
from string import digits

# Third party
import PySimpleGUI as psg

########################################################################
# CONSTANTS                                                            #
########################################################################

FRAME_PADDING = ((7,7), (7,13)) # ((L,R),(T,B)), is actually a margin


########################################################################
# DEFINITIONS                                                          #
########################################################################

def reduced_theme_list():
    """Removes duplicated themes (including the ones differing in a 
    trailing number) and the ones known to crash while testing the app."""
    # Bad themes caused the app to crash either because there's actually no
    # digitless version (key error), or something's wrong (?) with them
    # (background color number 123456789 error).
    bad_themes = [
        "Default",
        "DefaultNoMoreNagging",
        "GrayGrayGray",
        "LightGray",
        "Material",
        "SystemDefault",
        "SystemDefaultForReal"
    ]
    rtl = []
    for theme in psg.theme_list():
        digitless = theme.rstrip(digits)
        if digitless not in rtl and digitless not in bad_themes:
            rtl.append(digitless)
    return rtl


################################################################### MENU

menu_bar = [
    [
        "&File", 
        [
            "&New...",
            "&Open...",
            "&Save",
            "&Save as...",
            "Export as &PDF...",
            "E&xit"
        ]
    ],
    [
        "&Window",
        [
            "&Theme",
            [ f"Colour theme {theme}" for theme in reduced_theme_list() ]
        ]
    ],
    [
        "&Help",
        [
            "&About",
            "Project &webpage",
            "&Licenses",
            [
                "L&GPL (Lexpycon and PySimpleGUI)",
                "&MIT (TOMLkit)"
            ]
        ]
    ],
]


#################################################################### TOP

layout_top = [
    [psg.Frame(
        "Dictionary",
        [
            [
                psg.Column(
                    [
                        [
                            psg.Text("Lexpycon", font = ("mono", 20)),
                            psg.Text("", key = "-TITLE-", font = ("mono", 20))
                        ]
                    ],
                    vertical_alignment = "top"
                ),
                psg.Column(
                    [
                        [
                            psg.Text("Author"),
                            psg.Input(
                                "",
                                key = "-I-T-AUTHOR-",
                                size = 21,
                                expand_x = True,
                                metadata = {"attr": "config.author"}
                            ),
                        ],
                        [
                            psg.Text("Language name"),
                            psg.Input(
                                "",
                                key = "-I-T-LANGNAME-",
                                size = 21,
                                expand_x = True,
                                metadata = {"attr": "config.conlang_name"}
                            ),
                        ],
                        [
                            psg.Text("Language description"),
                            psg.Multiline(
                                "",
                                key = "-I-T-DESC-",
                                size = (25, 4),
                                metadata = {"attr": "config.conlang_description"}
                            ),
                        ]
                    ],
                    vertical_alignment = "top"
                ),
                psg.Column(
                    [
                        [
                            psg.Text("Named periods")
                        ],
                        [
                            psg.Table(
                                [[]],
                                headings = [
                                    "Period",
                                    "From",
                                    "Until"
                                ],
                                size = (45, 5),
                                key = "-T-T-PERIODS-",
                                auto_size_columns = False,
                                col_widths = [15,15,15],
                                display_row_numbers = False,
                                justification = "left",
                                enable_events = True,
                                enable_click_events = True
                            )
                        ]
                    ],
                    vertical_alignment = "top"
                ),
                psg.Column(
                    [
                        [
                            psg.Button(
                                "Edit",
                                key = "-B-PEDIT-",
                                disabled = False,
                                enable_events = True
                                )
                        ],
                        [
                            psg.Frame(
                                "Period",
                                [
                                    [
                                        psg.Button(
                                            "Add",
                                            key = "-B-PADD-"
                                            )
                                    ],
                                    [
                                        psg.Button(
                                            "Remove",
                                            key = "-B-PDEL-",
                                            disabled = True
                                            )
                                    ]
                                ],
                                key = "-F-PEDIT-",
                                visible = False
                            )
                        ]
                    ],
                    vertical_alignment = "top"
                )
            ]
        ],
        pad = FRAME_PADDING,
        expand_x = True,
        vertical_alignment = "top"
    )]
]


################################################################### LEFT


layout_left = [
    [psg.Frame(
        "Lexicon",
        [
            [
                psg.Text("Search"),
                psg.Input("", key = "-I-L-SEARCH-", enable_events = True)
            ],
            [
                psg.Checkbox(
                    "in headwords.",
                    default = True,
                    key = "-C-L-SHW-",
                    enable_events = True
                ),
                psg.Checkbox(
                    "in meanings.",
                    key = "-C-L-SMN-",
                    enable_events = True
                ),
                psg.Checkbox(
                    "match case.",
                    key = "-C-L-SCS-",
                    enable_events = True
                ),
            ],
            [
                psg.Table(
                    values = [[]],
                    headings = ["ID", "Headword", "Period", "Meaning"],
                    visible_column_map = [False, True, True, True],
                    auto_size_columns = False,
                    col_widths = [1,15,20,20],
                    display_row_numbers = False,
                    justification = "left",
                    key = "-LEXTABLE-",
                    selected_row_colors = "black on yellow",
                    enable_events = True,
                    enable_click_events = True,
                    expand_y = True,
                    font = ("mono", 10)
                )
            ],
            [
                psg.Button("New", key = "-B-NEW-"),
                psg.Button("Remove", key = "-B-DEL-")
            ]
        ],
        pad = FRAME_PADDING,
        title_location = "nw",
        expand_y = True
    )]
]


################################################################## RIGHT


layout_right = [
    [psg.Frame(
        "Lemma",
        [
            [
                psg.Text("Entry ID"),
                psg.Input(
                    key = "-I-R-ID-",
                    size = 16,
                    metadata = {"attr": "entry_id"}
                )
            ],
            [
                psg.Text("Headword"),
                psg.Input(
                    key = "-I-R-HEADW-",
                    expand_x = True,
                    metadata = {"attr": "lemma.headword"}
                )
            ],
            [
                psg.Text("Pronunciation"),
                psg.Input(
                    key = "-I-R-IPA-",
                    expand_x = True,
                    metadata = {"attr": "lemma.ipa"}
                )
            ],
            [
                psg.Text("Meaning"),
                psg.Multiline(
                    key = "-I-R-MEANING-",
                    size = (None, 2),
                    expand_x = True,
                    metadata = {"attr": "lemma.meaning"}
                )
            ],
            [
                psg.Frame(
                    "Grammar",
                    [
                        [
                            psg.Text("Part of speech"),
                            psg.Input(
                                key = "-I-R-POS-",
                                expand_x = True,
                                metadata = {"attr": "lemma.p_o_s"}
                            ),
                            psg.Combo(
                                [],
                                size = 15,
                                auto_size_text = True,
                                enable_events = True,
                                readonly = True,
                                visible = False,
                                key = "-O-R-POS-")
                        ],
                        [
                            psg.Table(
                                values = [[]],
                                headings = ["Features", ""],
                                auto_size_columns = True,
                                display_row_numbers = False,
                                justification = "left",
                                key = "-T-R-FEATS-",
                                selected_row_colors = "black on yellow",
                                enable_events = True,
                                enable_click_events = True,
                                num_rows = 5,
                                expand_x = True
                            ),
                            psg.Multiline(
                                "",
                                key = "-M-R-GRAMMAR-",
                                size = 20,
                                expand_x = True,
                                expand_y = True,
                                metadata = {"attr": "lemma.grammar_notes"}
                            )
                        ]
                    ],
                    expand_x = True
                )
            ],
            [
                psg.Frame(
                    "Usage period",
                    [
                        [
                            psg.Text("Usage from"),
                            psg.Input(
                                key = "-I-R-FROM-",
                                size = 10,
                                metadata = {"attr": "lemma.usage_from"}
                            ),
                            psg.Text("until"),
                            psg.Input(
                                key = "-I-R-UNTIL-",
                                size = 10,
                                metadata = {"attr": "lemma.usage_until"}
                            ),
                            psg.Text("", key = "-T-R-PERIOD-", expand_x = True)
                        ]
                    ],
                    expand_x = True
                )
            ],
            [
                psg.Frame(
                    "Etymology",
                    [
                        [
                            psg.Multiline(
                                key = "-M-R-ETYNOTES-",
                                size = (None, 3),
                                expand_x = True,
                                metadata = {"attr": "lemma.ety_notes"}
                            )
                        ],
                        [
                            psg.Tree(
                                data = psg.TreeData(),
                                headings = ["Period", "Meaning"],
                                key = "-I-R-ETYTREE-",
                                justification = "left",
                                enable_events = True,
                                show_expanded = True,
                                expand_x = True
                            )
                        ]
                    ],
                    expand_x = True
                )
            ],
            [
                psg.Button(
                    "Edit",
                    key = "-B-EDIT-",
                    disabled = True,
                    enable_events = True
                ),
                psg.Frame(
                    "Feature",
                    [
                        [
                            psg.Button(
                                "Add",
                                key = "-B-FEATADD-"
                            ),
                            psg.Button(
                                "Remove",
                                key = "-B-FEATDEL-",
                                disabled = True
                            )
                        ]
                    ],
                    key = "-F-EDIT-FEAT-",
                    title_location = "w",
                    visible = False
                ),
                psg.Frame(
                    "Etymon",
                    [
                        [
                            psg.Button(
                                "Add",
                                key = "-B-ETYADD-"
                            ),
                            psg.Button(
                                "Remove",
                                key = "-B-ETYDEL-",
                                disabled = True
                            )
                        ]
                    ],
                    key = "-F-EDIT-ETY-",
                    title_location = "w",
                    visible = False
                )
            ]
        ],
        pad = FRAME_PADDING,
        title_location = "nw",
    )]
]


########################################################################
# MAIN                                                                 #
########################################################################

main_layout = [
    [psg.Menu(menu_bar)],
    layout_top,
    [
        psg.Column(layout_left, element_justification = "left", expand_y = True),
        psg.Column(layout_right, element_justification = "left")
    ],
    [
        psg.Text(
            "",
            key = "-L-T-FILE-"
        ),
        psg.Sizegrip()
    ]
]


########################################################################
########################################################################


def main():
    """I don't really know if I need a main() funtion here."""
    print("Layout file fot Lexpycon. To be imported.")


if __name__ == '__main__':
    main()
