#!/usr/bin/env python3

# From https://github.com/python-poetry/poetry/issues/144#issuecomment-877835259

# Sandard library
import tomllib
from pathlib import Path

# Local
import lexpycon

def test_versions_are_in_sync():
    """Checks if the pyproject.toml and package.__init__.py __version__ are in sync."""

    path = Path(__file__).resolve().parents[1] / "pyproject.toml"
    pyproject = tomllib.loads(open(str(path)).read())
    pyproject_version = pyproject["tool"]["poetry"]["version"]

    package_init_version = lexpycon.__version__
    
    assert package_init_version == pyproject_version